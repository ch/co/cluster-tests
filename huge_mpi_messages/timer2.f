      program timer
*      use mpi
      implicit none
      include 'mpif.h'
      integer mx,my,niter
      parameter (niter=5)
      integer i,j,k,l
      real*8, allocatable, dimension(:,:) :: old1,old2,new1,new2
      real*8 av_trans,min_trans
      real*8 max_trans,znak
      real*8 time_trans
      real*8 time1,time2,all_trans
      integer ierror,rank,size,from,to,tag,mycomm,cartcomm
      integer ndims,dims(1),status(MPI_STATUS_SIZE),rank1,request
      logical periods(1),reorder
      character(len=32) :: arg

      call get_command_argument(1,arg)
      read(arg,'(i32)') mx
      call get_command_argument(2,arg)
      read(arg,'(i32)') my


      all_trans=0.d0

* initialize mpi, determine the number of processors and ranks
      call MPI_INIT(ierror)
      mycomm=MPI_COMM_WORLD
      call MPI_COMM_RANK(mycomm,rank,ierror)
      call MPI_COMM_SIZE(mycomm,size,ierror)
* create cartesian structure: ndims=1  determines a chain
      ndims=1
      dims(1)=0
      call MPI_DIMS_CREATE(size,ndims,dims,ierror)
* dims(1) contains number of processors in the chain (should equal size)
* non-periodic boundary conditions
      periods(1)=.false.
* allow reordering of ranks in order to optimize
      reorder=.true.
* create the new structure in cartcomm
      call MPI_CART_CREATE(mycomm,ndims,dims,periods,reorder
     & ,cartcomm,ierror)
* determine new ranks
      call MPI_COMM_RANK(cartcomm,rank1,ierror)
* determine on each processor the rank of the shifted processor
* 0 is the dimension in which shift takes place [0,ndims-1], 1 is shift
      call MPI_CART_SHIFT(cartcomm,0,1,from,to,ierror)
   
      if (rank1.eq.size/2) then
          write(*,*) '#mx, mx, iterations, size, av time, total time'
      end if
 
          all_trans=0.d0
          allocate(old1(mx,my))
          allocate(old2(mx,my))
          allocate(new1(mx,my))
          allocate(new2(mx,my))
* create initial something
          do i=1,mx
          do j=1,my
             old1(i,j)=dble(rank1)+1.d0
             old2(i,j)=dble(rank1)+1.d0+dble(size)
             new1(i,j)=0.d0
             new2(i,j)=0.d0
          enddo
          enddo
           do k=1,niter
   
              tag=0
    
* measure time
              time1=MPI_WTIME()
* send in forward direction
              call MPI_ISEND(old1,mx*my,MPI_DOUBLE_PRECISION,to,tag,cartcomm,request,ierror)
              call MPI_RECV(new1,mx*my,MPI_DOUBLE_PRECISION,from,tag,cartcomm,status,ierror)
              call MPI_WAIT(request,status,ierror)
    
* send in back direction direction
              call MPI_ISEND(old2,mx*my,MPI_DOUBLE_PRECISION,from,tag,cartcomm,request,ierror)
              call MPI_RECV(new2,mx*my,MPI_DOUBLE_PRECISION,to,tag,cartcomm,status,ierror)
              call MPI_WAIT(request,status,ierror)
              time2=MPI_WTIME()
    
              time_trans=time2-time1
              all_trans=all_trans+time_trans
    
c should not deallocate....bad with IB pinned memory
c              call MPI_BARRIER(cartcomm)
          enddo
c          deallocate(old1)
c          deallocate(old2)
c          deallocate(new1)
c          deallocate(new2)
           
          if (rank1.eq.size/2) then
             av_trans=all_trans/niter
             write(*,2) mx,my,mx*my,niter,size,av_trans,all_trans
          endif

       
      call MPI_FINALIZE(ierror)

      stop
1     format(i5,2f22.8)
2     format(5i10,2f20.10)
3     format(a12,f20.10)
      end

