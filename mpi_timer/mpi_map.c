/* This is a very crude program for experimenting with the map generation
codes without having to bother with everything else.  Its syntax is:

    mpi_map <simplex|duplex> procs [ localprocs [ technique ] ]
    mpi_seq procs [ localprocs [ technique ] ]
    mpi_cast procs [ localprocs ]

'localprocs' is the unit of local rather than remote access; naturally,
it must subdivide the number of processes and be greater than one.  Note
that it need not be the number of processes in a SMP unit, but could be
the number of CPUs on a board or any other relevant division.  If it is
negative, the processes are allocated round-robin rather than
contiguously.

technique controls alltoall emulations and is one of 'passes', 'total',
'peak' and 'overall' for minimising the number of passes, the maximum
total remote transfers per pass, the maximum remote transfers per pass
per node or the maximum overall transfers per pass.  The default is
'passes'. */



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#define COUNT(x)   (sizeof(x)/sizeof((x)[0]))
#define MAXSCALE   2     /* This may be set from 1 to (say) 5 */
#define MAXPASSES  (2*MAXSCALE+1)
static int trace = 0;    /* Set this to 1 or 2 for tracing. */



typedef struct {
    const char *tag;
} argument_type;

static int worldprocs = -1, localprocs = 0, power2procs = -1, worldid = -1,
    mapmethod = -1, mapprocs = -1, simplexmaplen = -1, simplexmapsize = -1,
    duplexmaplen = -1, duplexmapsize = -1, bcastmaplen = -1, bcastmapsize = -1,
    *cpulist = NULL, (*mappairs)[2] = NULL, *maptotal = NULL, *mapext = NULL,
    *mapoverall = NULL, *simplexsendmap = NULL, *simplexrecvmap = NULL,
    *duplexmap = NULL, *sequence = NULL, (*bcastpairs)[2] = NULL,
    *bcastsendmap = NULL, *bcastrecvmap = NULL;
static double mapeff[4][2] =
    {{-1.0,-1.0},{-1.0,-1.0},{-1.0,-1.0},{-1.0,-1.0}};
static argument_type global_method = {""}, global_grouping = {""},
    global_buffering = {""};



void failure (const char *message) {

/* Issue a minimal error message and stop. */

    fprintf(stderr,"Failure:  %s\n",message);
    exit(EXIT_FAILURE);
}



void syntax (void) {

/* Issue a message for an invokation error and stop. */

    fprintf(stderr,"Syntax:\n");
    fprintf(stderr,
        "    mpi_map <simplex|duplex> procs [ localprocs [ technique ] ]\n");
    fprintf(stderr,"    mpi_seq procs [ localprocs [ technique ] ]\n");
    fprintf(stderr,"    mpi_cast procs [ localprocs ]\n");
    exit(EXIT_FAILURE);
}



/* Include the common code. */

#include "mpi_mapping"



void handle_map (int argc, char *argv[], int doseq) {

/* Handle the alltoall code. */

    static const char *mapmethods[] = {"passes","total","peak","overall"};
    int mode, procs, i, m, n;
    char c;

/* Deal with the arguments. */

    if (! doseq) {
        mode = (strcmp(argv[1],"duplex") == 0);
        if (strcmp(argv[1],"simplex") != 0 && ! mode) syntax();
    } else {
        mode = 2;
        doseq = 1;
    }
    if (sscanf(argv[2-doseq],"%d%c",&procs,&c) != 1 || procs < 2 ||
            procs > 32768)
        failure("Invalid number of processes");
    worldprocs = procs;
    if (argc > 3-doseq) {
        if (sscanf(argv[3-doseq],"%d%c",&localprocs,&c) != 1 ||
                abs(localprocs) < 2 || abs(localprocs) >= procs ||
                procs%abs(localprocs) != 0)
            failure("Invalid localprocs argument");
        if (argc <= 4-doseq || strcmp(argv[4-doseq],mapmethods[0]) == 0)
            mapmethod = 0;
        else if (strcmp(argv[4-doseq],mapmethods[1]) == 0)
            mapmethod = 1;
        else if (strcmp(argv[4-doseq],mapmethods[2]) == 0)
            mapmethod = 2;
        else if (strcmp(argv[4-doseq],mapmethods[3]) == 0)
            mapmethod = 3;
        else
            failure("Invalid technique argument");
    } else {
        localprocs = procs;
        mapmethod = 0;
    }

/* Allocate space, generate the map and print out the results. */

    power2procs = 1;
    i = worldprocs-1;
    while (i > 0) {
        power2procs <<= 1;
        i >>= 1;
    }
    simplexmaplen = MAXPASSES*procs;
    duplexmaplen = MAXPASSES*((procs-1)/2+1);
    n = simplexmaplen*procs;
    m = simplexmaplen*((procs-1)/abs(localprocs)+1);
    if ((cpulist = malloc(procs*sizeof(int))) == NULL ||
            (mappairs = malloc(2*procs*procs*sizeof(int))) == NULL ||
            (maptotal = malloc(simplexmaplen*sizeof(int))) == NULL ||
            (mapext = malloc(m*sizeof(int))) == NULL ||
            (mapoverall = malloc(m*sizeof(int))) == NULL ||
            (simplexsendmap = malloc(n*sizeof(int))) == NULL ||
            (simplexrecvmap = malloc(n*sizeof(int))) == NULL ||
            (duplexmap = malloc(duplexmaplen*procs*sizeof(int))) == NULL ||
            (sequence = malloc(procs*sizeof(int))) == NULL)
        failure("Unable to allocate workspace");
    for (i = 0; i < procs; ++i) cpulist[i] = i;
    choosemap(procs,mode);
    displaymap(stdout,mode);
    fprintf(stderr,"Efficiencies\n");
    fprintf(stderr,"Total passes:  %.1f%% (best %.1f%%)\n",
        100.0*mapeff[0][0],100.0*mapeff[0][1]);
    fprintf(stderr,"Total external:  %.1f%% (best %.1f%%)\n",
        100.0*mapeff[1][0],100.0*mapeff[1][1]);
    fprintf(stderr,"Max. external:  %.1f%% (best %.1f%%)\n",
        100.0*mapeff[2][0],100.0*mapeff[2][1]);
    fprintf(stderr,"Max. aggregate:  %.1f%% (best %.1f%%)\n",
        100.0*mapeff[3][0],100.0*mapeff[3][1]);
}



void handle_cast (int argc, char *argv[]) {
    int procs, i;
    char c;

/* Deal with the arguments. */

    if (sscanf(argv[1],"%d%c",&procs,&c) != 1 || procs < 2 || procs > 32768)
        failure("Invalid number of processes");
    worldprocs = procs;
    if (argc > 2) {
        if (sscanf(argv[2],"%d%c",&localprocs,&c) != 1 ||
                abs(localprocs) < 2 || abs(localprocs) >= procs ||
                procs%abs(localprocs) != 0)
        failure("Invalid localprocs argument");
    } else
        localprocs = procs;

/* Allocate space, generate the map and print out the results.  Note
that mappairs need not be very big for this case. */

    power2procs = 1;
    i = worldprocs-1;
    while (i > 0) {
        power2procs <<= 1;
        i >>= 1;
    }
    bcastmaplen = procs*(int)(
        ceil(log(worldprocs/(double)localprocs)/log(2.0))+
        ceil((double)(log((double)localprocs)/log(2.0))));
    if ((cpulist = malloc(procs*sizeof(int))) == NULL ||
            (bcastpairs = malloc(2*procs*sizeof(int))) == NULL ||
            (bcastsendmap = malloc(bcastmaplen*sizeof(int))) == NULL ||
            (bcastrecvmap = malloc(bcastmaplen*sizeof(int))) == NULL)
        failure("Unable to allocate workspace");
    for (i = 0; i < procs; ++i) cpulist[i] = i;
    choosecast(procs);
    displaymap(stdout,3);
}



int main (int argc, char *argv[]) {
    int mode = -1;
    char *ptr;

/* Work out which mode we are in and call the relevant code. */

    if (argv[0] != NULL) {
        if ((ptr = strrchr(argv[0],'/')) == NULL) ptr = argv[0]; else ++ptr;
        if (strcmp(ptr,"mpi_map") == 0 && argc >= 3 && argc <= 5)
            mode = 1;
        else if (strcmp(ptr,"mpi_seq") == 0 && argc >= 2 && argc <= 4)
            mode = 2;
        else if (strcmp(ptr,"mpi_cast") == 0 && argc >= 2 && argc <= 3)
            mode = 3;
        else
            syntax();
    } else
        syntax();
    if (mode == 1)
        handle_map(argc,argv,0);
    else if (mode == 2)
        handle_map(argc,argv,1);
    else
        handle_cast(argc,argv);
    return 0;
}
