.TH mpi_timer 1
.SH NAME
mpi_timer \- a basic MPI-1 timing and checking program
.SH SYNOPSIS
.B mpi_timer
[
.BI \-trace " level"
]
.BI \-method " method"
[
.BI \-grouping " grouping"
] [
.BI \-buffering " buffering"
] [
.B \-allocate
] [
.BI \-time " time"
] [
.BI \-procs " procs"
] [
.BI \-minprocs " minprocs"
] [
.BI \-procfactor " procfactor"
] [
.BI \-localprocs " localprocs [ technique ]"
]
.BI \-size " size"
[
.BI \-minsize " minsize"
] [
.BI \-sizefactor " sizefactor"
]
.SH DESCRIPTION
.PP
.I mpi_timer
is an MPI timing program, that implements two forms of a ping-pong
cohort, six forms of all-to-all and three forms of broadcast, and times
each of them.  Some of its measurements will give point-to-point
performance, and others will give bisection.
.PP
Some effort has been put into the
.I MPI_Alltoall
optimisation, and it is
common for at least one of its variants to achieve better bandwidth than
vendors' built-in
.IR MPI_Alltoall .
If this gain is significant, the vendor
should do the obvious thing; appropriate credit would be appreciated.
.SS Options
.PP
.I mpi_timer
recognizes the following options:
.TP
.BI \-trace " level"
This is used to control diagnostic output and may be 0 (the default),
1 or 2.  The output is not pretty and is often unhelpful, as there are
many ways in which things can go wrong.
.TP
.BI \-method " method"
This may be one or more of the following letters or one of the
names in brackets:
.TP
.I "  (all)"
 use all of the following
.TP
.I "p (pairwise)"
use MPI_Ssend and MPI_Recv (cohort)
.TP
.I "i (ipairwise)"
use MPI_I?send and MPI_Irecv (cohort)
.TP
.I "a (alltoall)"
use MPI_Alltoall (all-to-all)
.TP
.I "g (gather)"
use MPI_Gather (all-to-all)
.TP
.I "s (scatter)"
use MPI_Scatter (all-to-all)
.TP
.I "h (handshake)"
use MPI_S?end/Recv pairwise (all-to-all)
.TP
.I "d (duplex)"
use MPI_I?send/Irecv pairwise (all-to-all)
.TP
.I "f (flipflop)"
use MPI_Sendrecv pairwise (all-to-all)
.TP
.I "r (roundrobin)"
use MPI_Sendrecv round-robin (all-to-all)
.TP
.I "m (multiplex)"
use MPI_Issend/Irecv free-for-all (all-to-all)
.TP
.I "b (broadcast)"
use MPI_Bcast (broadcast)
.TP
.I "t (tree)"
use MPI_S?end in a tree structure (broadcast)
.TP
.I "w (widetree)"
use MPI_I?send in a wide tree structure (broadcast)
.PP
This argument is compulsory.  Note that the cohort tests are simply a
parallel, pairwise ping-pong, which varies between point-to-point
and bisection, and all of the others are functionally equivalent to
either
.I MPI_Alltoall
or a
.I MPI_Bcast
from each process.
.TP
.BI \-grouping " grouping"
This may be one or more of the following letters or one of the
names in brackets:
.TP
.I "  (all)"
 use all of the following
.TP
.I "w (world)"
use just
.I MPI_COMM_WORLD
.TP
.I "c (contiguous)   -  subdivide by taking the first processes
.TP
.I "b (bisected)     -  subdivide by using the two halves
.TP
.I "d (distributed)  -  subdivide by distributing processes
.TP
.I "s (shuffled)     -  subdivide in a pseudo-random way
.PP           
The default is
.IR world ,
which uses all processes in a selection of orders.
.TP
.BI \-buffering " buffering"
This may be one or more of the following letters or one of the
names in brackets:
.TP
.I "  (all)"
use all of the following
.TP
.I "d (default)"
use default buffering (i.e.
.I MPI_Send
etc.)
.TP
.I "b (buffered)"
use buffered I/O (i.e.
.I MPI_Bsend
etc.)
.TP
.I "s (synchronous)"
use synchronous transfer (i.e.
.I MPI_Ssend
etc.)
.PP
The default is
.IR default .
If the method includes only letters
.IR ags ,
this may not be set to another value.
.TP
.B \-allocate
indicates that
.I MPI_buffer_attach
should be used for buffered I/O.  The default is not to.
.TP
.BI \-time " time"
This is the approximate number of seconds for which each test is to run,
where 0.001 <=
.I time
<= 300.0, and the program will adapt the repetition count appropriately.
The default is 1.0.
.TP
.BI \-procs " procs"
.TP
.BI \-minprocs " minprocs"
.TP
.BI \-procfactor " procfactor"
The number of processors starts at
.I procs
and is reduced by
.I procfactor
(0.0 <
.I procfactor
< 1.0) down to (but not below)
.IR minprocs .
The default for
.I minprocs
is 2 and for
.I procfactor
is
.IR 1/sqrt(2) .
Neither
.I procs
nor
.I minprocs
may be below 2 and
.I procfactor
must be reasonable in the context of them.
.TP
.BI \-localprocs " localprocs technique threads"
If this is set, the absolute value of
.I localprocs
is the number of processors in a 'node', and
.I technique
is one of
.IR passes ,
.IR total ,
.I peak
and
.IR overall .
A negative value of
.I localprocs
is used to indicate round-robin, rather than contiguous, allocation to nodes.
The optimisation is crude and empirical, but it doesn't seem to matter.
.I technique
controls alltoall emulations and is one of
.IR passes ,
.IR total ,
.I peak
and
.IR overall ,
for minimising the number of passes, the total external transfers per pass, the
peak external transfers per pass per node and the aggregate peak transfers per
pass per node.
.PP
Nonsensical combinations of the number of processors and the grouping are
errors.  These include
.I procs
less than
.I MPI_COMM_WORLD
and
.I grouping
.I world
(implicitly or explicitly),
.I grouping
including only
.I world
and
.I minprocs
less than
.IR procs ,
.I grouping
including
.I d
and
.I MPI_COMM_WORLD
not a power of two, and ineffective division factors.  Currently,
.I localprocs
is currently supported only for
.I grouping
.IR world ,
but only because the code to
support the other cases has not been written.
.TP
.BI \-size " size"
.TP
.BI \-minsize " minsize"
.TP
.BI \-sizefactor " sizefactor"
The
.I size
argument is compulsory.  The sizes may be of the form
.IR n ,
.I nK
or
.IR nM ,
where 1 <=
.I n
<= 65536, and specify transfer sizes of that number of kilobytes or megabytes;
they are rounded UP to a multiple of
the size of a double.  The size starts at
.I size
and is reduced by
.I sizefactor
(0.0 <
.I sizefactor
< 1.0) down to (but not below)
.IR minsize .
The defaults for
.IR minsize is
.IR sizeof(double)
and for
.I sizefactor
is
.IR 1/sqrt(2).
.IR sizefactor
must be reasonable in the context of
.I size
and
.IR minsize .
.SH OUTPUT           
The main output (written to
.IR stdout )
is a series of lines with the following data, printed only
from the root process:
.TP 
.I "      method  grouping  buffering    world_proc  local_procs"
.TP 
.I "      proc_count  packet_size    repeat_count  seconds"
.TP 
.I "      passes  calls_per_cpu  copies_per_cpu  self_per_cpu  otherpercpu"
.TP 
.I "      calls_per_node  copies_per_node  int_per_node  ext_per_node  external"
.PP
The first three figures are names as specifiable in the options for
the current timing test,
.I world_proc
is the size of
.IR MPI_COMM_WORLD ,
.I local_procs
is the number of processors in a `node',
.I proc_count
is the number of processes being used,
.I packet_size
is the size being used,
.I repeat_count
is the number of repetitions and
.I seconds
is the time in seconds that they took.
The values of
.IR passes ,
.IR calls_per_cpu ,
.IR copies_per_cpu ,
.IR self_per_cpu ,
.IR otherpercpu ,
.IR copies_per_node ,
.IR int_per_node ,
.I ext_per_node
and
.I external
are for the analysis program, and are all zero for
.I MPI_Bcast
because it isn't possible to 
give them meaningful values.  The second to fourth are maximised over
all cpus for each pass, and then summed; the fifth to seventh are
maximised over all nodes for each pass and then summed.  The eighth is
summed.  They are the MPI calls, copies of all sorts, copies to the
process itself, copies to other processes, the MPI calls, copies of all
sorts, copies to the node itself, copies to other nodes and all
inter-node copies.  All copies count one for each read and each write.
 .PP
The options actually used are written to
.IR stderr ,
plus a fair amount of debugging information if
.I trace
is set to 1 in the code and even more if it is set to 2.
.SH RETURN VALUE
The program returns a zero exit status for success, and a non-zero one
otherwise.  Most errors are fatal.
.SH BUGS
The program has NOT been debugged as thoroughly as it should be; in
case of trouble, setting
.I trace
to 1 or 2 (see the code) may
help.  It does have a fair amount of consistency checking. Perhaps
the most horrible aspect is the way that the assumptions of the
maximum number of passes are intertwined.

The optimisation is crude and empirical, and is better viewed as a way
of experimenting with alternate patterns of access.  It is not clear how
to do better in general, because machine models vary so widely.
.SH AUTHOR
.I mpi_timer
was developed by N.M. Maclaren of the University of Cambridge Computing
Service.
