#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (argc,argv)
int argc;
char *argv[];
{
    int myid, numprocs, namelen, i, n;
    char processor_name[MPI_MAX_PROCESSOR_NAME], *buffer;

    if (argc < 2) {
        fprintf(stderr,"Incorrect number of arguments\n");
        exit(EXIT_FAILURE);
    }
    n = 1;
    for (i = 1; i < argc; ++i) n += strlen(argv[i])+1;
    if ((buffer = malloc(n)) == NULL) {
        fprintf(stderr,"Unable to get space for command\n");
        exit(EXIT_FAILURE);
    }
    strcpy(buffer,argv[1]);
    for (i = 2; i < argc; ++i) {
        strcat(buffer," ");
        strcat(buffer,argv[i]);
    }

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    MPI_Get_processor_name(processor_name,&namelen);
    MPI_Barrier(MPI_COMM_WORLD);

    system(buffer);

    MPI_Finalize();
    return 0;
}
