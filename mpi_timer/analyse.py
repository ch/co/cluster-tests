from re import compile
from sys import argv, stdout, stderr, exit
from math import sqrt, log, exp

pat = compile(r'^(\w+) (\w+) (\w+) +(\d+) (\d+) +(\d+) (\d+) +(\d+) (\d+\.\d+) +( \d+\.\d+)+\n$')
split = 0
if argv[1] == "-split" :
    split = 1
    argv[1:] = argv[2:]
data = {}
prod = 0.0
nprod = 0

for name in argv[1:] :
    file = open(name,"r")
    procs = 0
    maxsize = 0
    mismatch = 0
    reject = 0
    accept = 0
    last = ""
    count = 0
    while 1 :
        line = file.readline()
        if not line :
            break
        if not pat.match(line) :
            mismatch = mismatch+1
            continue
        action = pat.sub(r'\1',line)
        if procs == 0 :
            procs = int(pat.sub(r'\4',line))
        size = int(pat.sub(r'\7',line))
        if pat.sub(r'\2',line) != "world" or \
                pat.sub(r'\3',line) != "default" or \
                int(pat.sub(r'\4',line)) != procs or \
                int(pat.sub(r'\6',line)) != procs :
            reject = reject+1
            continue
        accept = accept+1
        if size > maxsize :
            maxsize = size
        size = size*float(procs)
        time = float(pat.sub(r'\9',line))/int(pat.sub(r'\8',line))
        if action == "pairwise" or action == "ipairwise" :
            pass
        elif action == "alltoall" or action == "gather" or \
                action == "scatter" or action == "handshake" or \
                action == "duplex" or action == "flipflop" or \
                action == "roundrobin" or action == "multiplex" :
            size = size*procs
        elif action == "broadcast" or action == "tree" or \
                action == "widetree" :
            size = size*log(float(procs))/log(2.0)
        else :
            reject = reject+1
            continue
        if split :
            if action != last :
                last = action
                count = count+1
            action = action + "/" + str(count)
            prod = prod+log(size)
            nprod = nprod+1
        if not data.has_key(action) :
            data[action] = []
        data[action].append((procs,size,time))
    file.close()
    stdout.write("%s:  mismatch = %d, reject = %d, " % (name,mismatch,reject))
    stdout.write("accept = %d, procs = %d, max size = %d\n" % \
            (accept,procs,maxsize))
stdout.write("\n")

if split :
    prod = exp(prod/nprod)
speeds = []
times = []
for action in data.keys() :
    av = 0.0
    var = 0.0
    weight = 0
    for item in data[action] :
        if not split or item[1] > prod :
            w = item[1]*item[1]
            speed = item[1]/item[2]
            weight = weight+w
            av = av+speed*w
            var = var+speed*speed*w
    if weight > 0 :
        speed = av/weight
        speed_err = sqrt(abs(var/weight-av*av/(weight*weight)))
        speeds.append((speed,speed_err,action))
    av = 0.0
    var = 0.0
    weight = 0
    for item in data[action] :
        if not split or item[1] < prod :
            w = 1.0/(item[1]*item[1])
            time = item[2]
            weight = weight+w
            av = av+time*w
            var = var+time*time*w
    if weight > 0 :
        time = av/weight
        time_err = sqrt(abs(var/weight-av*av/(weight*weight)))
        times.append((time,time_err,action))
speeds.sort()
speeds.reverse()
for x in speeds :
    stdout.write("%-16s    %.2f += %.2f GB/sec\n" % \
        (x[2],1.0e-9*x[0],2.0e-9*x[1]))
stdout.write("\n")
times.sort()
for x in times :
    stdout.write("%-16s    %.1f += %.1f microsecs\n" % \
        (x[2],1.0e6*x[0],2.0e6*x[1]))
