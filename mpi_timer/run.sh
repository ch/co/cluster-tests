# Just an example of how you might run this. 
# This one works on hathor.
#PBS -N mpi_timer
#PBS -q ivybridge-fast
#PBS -l nodes=node007:ppn=16+node008:ppn=16

source /etc/profile.d/modules.sh
module add gcc/gcc-4.9.1
module add openmpi/1.8.2/gcc-4.9.1

cd $PBS_O_WORKDIR
outfile=${PBS_O_WORKDIR}/timerout.${PBS_JOBID}

cat $PBS_NODEFILE > $outfile
mpirun ./mpi_timer -size 64 -method all >> $outfile
mpirun ./mpi_timer -size 256K -minsize 64K -method all >> $outfile
