/* This is an MPI timing program, that implements two forms of a
ping-pong cohort, six forms of all-to-all and three forms of broadcast,
and times each of them.  Some of its measurements will give
point-to-point performance, and others will give bisection.

Some effort has been put into the MPI_Alltoall optimisation, and it is
common for at least one of its variants to achieve better bandwidth than
vendors' built-in MPI_Alltoall.  If this gain is significant, the vendor
should do the obvious thing; appropriate credit would be appreciated.
There are several other possible variations that might be better under
some circumstances, but there came a point where I had to stop fiddling.

The MPI_Bcast optimisation is less likely to be beneficial, because the
optimum solution typically involves primitives that are not available in
MPI (e.g. shareable data).  This is one of the few cases where
single-sided communication might perform better.

It is called by:

    mpi_timer [ -trace level ] -method method [ -grouping grouping ]
        [ -buffering buffering ] [ -allocate ] [ -time time ]
        [ -procs procs ] [ -minprocs minprocs ] [ -procfactor procfactor ]
        [ -localprocs localprocs [ technique ] ]
        -size size [ -minsize minsize ] [ -sizefactor sizefactor ]

In all of this, letters may be in either case.

level is used to control diagnostic output and may be 0 (the default),
1 or 2.  The output is not pretty and is often unhelpful, as there are
many ways in which things can go wrong.

method may be one or more of the following letters or one of the
names in brackets:
      (all)          -  use all of the following
    p (pairwise)     -  use MPI_Ssend and MPI_Recv (cohort)
    i (ipairwise)    -  use MPI_I?send and MPI_Irecv (cohort)
    a (alltoall)     -  use MPI_Alltoall (all-to-all)
    g (gather)       -  use MPI_Gather (all-to-all)
    s (scatter)      -  use MPI_Scatter (all-to-all)
    h (handshake)    -  use MPI_S?end/Recv pairwise (all-to-all)
    d (duplex)       -  use MPI_I?send/Irecv pairwise (all-to-all)
    f (flipflop)     -  use MPI_Sendrecv pairwise (all-to-all)
    r (roundrobin)   -  use MPI_Sendrecv round-robin (all-to-all)
    m (multiplex)    -  use MPI_Issend/Irecv free-for-all (all-to-all)
    b (broadcast)    -  use MPI_Bcast (broadcast)
    t (tree)         -  use MPI_S?end in a tree structure (broadcast)
    w (widetree)     -  use MPI_I?send in a wide tree structure (broadcast)

This argument is compulsory.  Note that the cohort tests are simply a
parallel, pairwise ping-pong, which varies between point-to-point
and bisection, and all of the others are functionally equivalent to
either MPI_Alltoall or a MPI_Bcast from each process.

grouping may be one or more of the following letters or one of the
names in brackets:
      (all)          -  use all of the following
    w (world)        -  use just MPI_COMM_WORLD
    c (contiguous)   -  subdivide by taking the first processes
    b (bisected)     -  subdivide by using the two halves
    d (distributed)  -  subdivide by distributing processes
    s (shuffled)     -  subdivide in a pseudo-random way

The default is 'world', which uses all processes in a selection of
orders.

buffering may be one or more of the following letters or one of the
names in brackets:
      (all)          -  use all of the following
    d (default)      -  use default buffering (i.e. MPI_Send etc.)
    b (buffered)     -  use buffered I/O (i.e. MPI_Bsend etc.)
    s (synchronous)  -  use synchronous transfer (i.e. MPI_Ssend etc.)

The default is 'default'.  If the method includes only letters 'ags',
this may not be set to another value.

allocate indicates that MPI_buffer_attach should be used for buffered
I/O.  The default is not to.

The time is the approximate number of seconds for which each test is to
run, where 0.001 <= time <= 300.0, and the program will adapt the
repetition count appropriately.  The default is 1.0.

The number of processors starts at procs and is reduced by procfactor
(0.0 < procfactor < 1.0) down to (but not below) minprocs.  The default
for minprocs is 2 and for procfactor is 1/sqrt(2).  Neither procs nor
minprocs may be below 2 and procfactor must be reasonable in the context
of them.

If it is set, the absolute value of localprocs is the number of
processors in a 'node'; a negative value of localprocs is used to
indicate round-robin, rather than contiguous, allocation to nodes.  The
optimisation is crude and empirical, but it doesn't seem to matter.

technique controls alltoall emulations and is one of 'passes', 'total',
'peak' and 'overall', for minimising the number of passes, the total
external transfers per pass, the peak external transfers per pass per
node and the aggregate peak transfers per pass per node.  The default is
'passes'.

Nonsensical combinations of the number of processors and the grouping
are errors.  These include procs less than MPI_COMM_WORLD and grouping
world (implicitly or explicitly), grouping including only world and
minprocs less than procs, grouping including 'd' and MPI_COMM_WORLD not
a power of two, and ineffective division factors.  Currently, localprocs
is currently supported only for grouping world, but only because the
code to support the other cases has not been written.

The size argument is compulsory.  The sizes may be of the form 'n', 'nK'
or 'nM', where 1 <= n <= 65536, and specify transfer sizes of that
number of kilobytes or megabytes; they are rounded UP to a multiple of
the size of a double.  The size starts at size and is reduced by
sizefactor (0.0 < sizefactor < 1.0) down to (but not below) minsize.
The defaults for minsize and for sizefactor are sizeof(double) and
1/sqrt(2).  sizefactor must be reasonable in the context of size and
minsize.

The main output is a series of lines with the following data, printed
only from the root process:

    method  grouping  buffering    world_procs  local_procs
    proc_count  packet_size    repeat_count  seconds
    passes  calls_per_cpu  copies_per_cpu  self_per_cpu  otherpercpu
    calls_per_node  copies_per_node  int_per_node  ext_per_node  external

The values of passes, calls_per_cpu, copies_per_cpu, self_per_cpu,
otherpercpu, copies_per_node, int_per_node, ext_per_node and external
are for the analysis program, and are all zero for MPI_Bcast because it
isn't possible to give them meaningful values.  The second to fourth are
maximised over all cpus for each pass, and then summed; the fifth to
seventh are maximised over all nodes for each pass and then summed.  The
eighth is summed.  They are the MPI calls, copies of all sorts, copies
to the process itself, copies to other processes, the MPI calls, copies
of all sorts, copies to the node itself, copies to other nodes and all
inter-node copies.  All copies count one for each read and each write.
            
The program has NOT been debugged as thoroughly as it should be; in
case of trouble, setting the symbol trace to 1 or 2 (see below) may
help.  It does have a fair amount of consistency checking. Perhaps
the most horrible aspect is the way that the assumptions of the
maximum number of passes are intertwined with the logic. */



#include "mpi.h"
#include <ctype.h>
#include <limits.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>
#ifdef USE_GETCPUID
    extern unsigned int getcpuid (int worldid);
#endif

#define COUNT(x)   (sizeof(x)/sizeof((x)[0]))
#define MAXSCALE   2        /* This may be set from 1 to (say) 5 */
#define MAXPASSES  (2*MAXSCALE+1)
#define MINBUFFER  65536    /* Minimum size for MPI_Buffer_attach */
#define MARGIN     64       /* Checking margin for allocation */
#define WASTE       5       /* Time to waste at start-up - see iterate */



/* Define some structure arrays to keep the bulk of the code generic. */

typedef struct {
    const char *tag;
} argument_type;

typedef struct {
    const char *tag;
    void (*method) (double [], double [], size_t, int, int,
        MPI_Comm, int, int);
    int alltoall;
} method_type;

typedef argument_type grouping_type;

typedef struct {
    const char *tag;
    int (*send) (void *, int, MPI_Datatype, int, int, MPI_Comm);
    int (*isend) (void *, int, MPI_Datatype, int, int, MPI_Comm,
        MPI_Request *);
    const char *send_tag, *isend_tag;
} buffering_type;

void pairwise (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    ipairwise (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    alltoall (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    gather (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    scatter (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    handshake (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    duplex (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    flipflop (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    roundrobin (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    multiplex (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    broadcast (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    tree (double[], double[], size_t, int, int, MPI_Comm, int, int), 
    widetree (double[], double[], size_t, int, int, MPI_Comm, int, int);

static const method_type methods[] = {
    {"pairwise", pairwise, 0},
    {"ipairwise", ipairwise, 0},
    {"alltoall", alltoall, 1},
    {"gather", gather, 1},
    {"scatter", scatter, 1},
    {"handshake", handshake, 1},
    {"duplex", duplex, 1},
    {"flipflop", flipflop, 1},
    {"roundrobin", roundrobin, 1},
    {"multiplex", multiplex ,1},
    {"broadcast", broadcast, 2},
    {"tree", tree, 2},
    {"widetree", widetree, 2}
};
static const char *method_all = "piagshdfrmbtw";

static const grouping_type groupings[] = {
    {"world"},
    {"contiguous"},
    {"bisected"},
    {"distributed"},
    {"shuffled"}
};
static const char *grouping_all = "wcbds";

static buffering_type bufferings[] = {
    {"default", MPI_Send, MPI_Isend, "MPI_Send", "MPI_Isend"},
    {"buffered", MPI_Bsend, MPI_Ibsend, "MPI_Bsend", "MPI_Ibsend"},
    {"synchronous", MPI_Ssend, MPI_Issend, "MPI_Ssend", "MPI_Issend"}
};
static const char *buffering_all = "dbs";

typedef struct {
    double passes, callspercpu, copiespercpu, selfpercpu, otherpercpu,
        callspernode, copiespernode, intpernode, extpernode, external;
} analysis_type;

typedef struct CHAIN {
    const char *tag;
    struct CHAIN *next;
    size_t size;
    unsigned long checks;
    double dummy1;
    void * dummy2;
} chain;



/* Store some global data here to avoid messy code.  Yes, I know that
this isn't the way to do it, and is messy in other ways :-( */

static void *allocated = NULL;
static int margin = -1, trace = -1, worldprocs = -1, localprocs = 0,
    power2procs = -1, startprocs = -1, worldid = -1, minprocs = -1,
    mapmethod = -1, mapprocs = -1, simplexmaplen = -1, simplexmapsize = -1,
    duplexmaplen = -1, duplexmapsize = -1, bcastmaplen = -1, bcastmapsize = -1,
    veryfirst = 1, *cpulist = NULL, (*mappairs)[2] = NULL, *maptotal = NULL,
    *mapext = NULL, *mapoverall = NULL, *simplexsendmap = NULL,
    *simplexrecvmap = NULL, *duplexmap = NULL, *sequence = NULL,
    (*bcastpairs)[2] = NULL, *bcastsendmap = NULL, *bcastrecvmap = NULL;
static double mapeff[4][2] =
    {{-1.0,-1.0},{-1.0,-1.0},{-1.0,-1.0},{-1.0,-1.0}};
static method_type global_method = {NULL,NULL,-1};
static grouping_type global_grouping = {NULL};
static buffering_type global_buffering = {NULL,NULL,NULL};
static size_t startsize = 0, minsize = 0;
static double testtime = -1.0, procfactor = -1.0, sizefactor = -1.0;
static MPI_Status *global_status = NULL;
static MPI_Request *global_request = NULL;
static analysis_type analysis, anal_datum, *anal_cpus = NULL,
    *anal_nodes = NULL;



void failure (const char *message) {

/* Issue a minimal error message and stop. */

    fprintf(stderr,"Failure:  %s\n",message);
    if (MPI_Finalize()) {
        fprintf(stderr,"Unable to call MPI_Finalize\n");
        exit(EXIT_FAILURE);
    }
    exit(EXIT_FAILURE);
}



/* Include the common code. */

#include "mpi_common"
#include "mpi_mapping"



void *myalloc (const char *tag, size_t size) {

/* A checking version of malloc, to attempt to detect array overflow
problems in my code.  It is pretty horrible, but should be portable
except to the most perverse implementations. */

    chain *ptr;
    int i, n;
    unsigned long *p, z;

/* Create the margin, aligned close to maximally, allocate the memory
and initialise the checking fields. */

    if (margin < 0)
        margin = ((MARGIN-1)/sizeof(chain)+1)*sizeof(chain);
    size = ((size-1)/sizeof(chain)+1)*sizeof(chain);
    if ((ptr = malloc(size+2*margin)) == NULL) return NULL;
    ptr->tag = tag;
    ptr->next = allocated;
    ptr->size = size;
    if (trace)
        fprintf(stderr,"Allocating %s at %p (%ld long)\n",
            tag,(void *)((char *)ptr+margin),(long)size);
    n = margin/sizeof(long);
    allocated = ptr;
    z = ((unsigned long *)ptr)[0]^123456789;
    for (i = offsetof(chain,checks)/sizeof(long); i < n; ++i)
        ((unsigned long *)ptr)[i] = (z *= 62748517);
    p = (unsigned long *)((char *)ptr+ptr->size+margin);
    for (i = n-1; i >= 0; --i) p[i] = (z *= 62748517);
    return (char *)ptr+margin;
}



void checkalloc (void) {

/* See if any of the allocations have got corrupted. */

    chain *ptr;
    int i, n;
    unsigned long *p, z;

/* This is the dual of creation, in a loop */

    n = margin/sizeof(long);
    ptr = allocated;
    while (ptr != NULL) {
        z = ((unsigned long *)ptr)[0]^123456789;
        for (i = offsetof(chain,checks)/sizeof(long); i < n; ++i)
            if (((unsigned long *)ptr)[i] != (z *= 62748517)) {
                fprintf(stderr,"%d elements before %s corrupted",margin-i);
                failure("Failed in checkalloc");
            }
        p = (unsigned long *)((char *)ptr+ptr->size+margin);
        for (i = n-1; i >= 0; --i)
            if (p[i] != (z *= 62748517)) {
                fprintf(stderr,"%d elements after %s corrupted",i);
                failure("Failed in checkalloc");
            }
        ptr = ((chain *)ptr)->next;
    }
}



void myfree (void) {

/* See if any of the allocations have got corrupted and free all of
them. */

    void *ptr, *next;

    checkalloc();
    ptr = allocated;
    while (ptr != NULL) {
        next = ((chain *)ptr)->next;
        free(ptr);
        ptr = next;
    }
}



double randy (void) {

/* Return a U(0,1) pseudo-random number.  A pox on the numerous systems
that pollute the namespace with bad generators called 'random'. */

    static unsigned long seed = 123;;

    seed = (seed*62748517)&0xffffffff;
    return (double)seed/4294967296.0;
}



int shuffle (int previous, int procs) {

/* Return a pseudo-random integer, without repeating.  This is NOT how
to do it, but is good enough for this purpose! */

    while ((previous = (2197*previous+1)%power2procs) >= procs) ;
    return previous;
}



void reset_analysis (analysis_type *datum) {

/* Initialise the analysis data structure. */

    datum->passes = datum->callspercpu = datum->copiespercpu =
        datum->selfpercpu = datum->otherpercpu = datum->callspernode =
        datum->copiespernode = datum->intpernode = datum->extpernode =
        datum->external = 0.0;
}



void update_analysis (int procs, int procid, int target, int passes,
    int calls, int copies) {

/* Update analysis directly for the simple counts, and datum for those
we need to take the maximum over cpus or nodes for. */

    int node = getnode(procid), i;

    analysis.passes += passes;
    anal_datum.callspercpu += calls;
    anal_datum.callspernode += calls;
    if (target >= 0) {
        anal_datum.copiespercpu += copies;
        anal_datum.copiespernode += copies;
        if (target == procid) {
            anal_datum.selfpercpu += copies;
        } else {
            anal_datum.otherpercpu += copies;
            if (getnode(target) == node)
                anal_datum.intpernode += copies;
            else {
                anal_datum.extpernode += copies;
                anal_datum.external += copies;
            }
        }
    } else if (copies > 0) {
        for (i = 0; i < procs; ++i) {
            if (i == procid) continue;
            anal_datum.copiespercpu += copies;
            anal_datum.copiespernode += copies;
            anal_datum.otherpercpu += copies;
            if (getnode(i) == node)
                anal_datum.intpernode += copies;
            else {
                anal_datum.extpernode += copies;
                anal_datum.external += copies;
            }
        }
    }
}



void gather_analysis (MPI_Comm comm, int procs, int procid) {

/* Summarise the various counts.  This must be called as a collective. */

    int nodes = (worldprocs-1)/abs(localprocs)+1, i, n;

/* Gather all the data together to process zero, upon completing a pass.
Note that this will copy uninitialised data if the implementation pads
structures consisting purely of doubles.  Sad. */

    ++analysis.passes;
    if (trace > 1)
        fprintf(stderr,
            "analysis(1):  %s %s %s %d %d    %d %d    %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f\n",
            global_buffering.tag,global_method.tag,global_grouping.tag,
            worldprocs,worldid,procs,procid,analysis.passes,
            analysis.callspercpu,anal_datum.copiespercpu,
            anal_datum.selfpercpu,anal_datum.otherpercpu,
            analysis.callspernode,anal_datum.copiespernode,
            anal_datum.intpernode,anal_datum.extpernode,anal_datum.external);
    n = sizeof(analysis_type)/sizeof(double);
    if (MPI_Gather(&anal_datum,n,MPI_DOUBLE,anal_cpus,n,MPI_DOUBLE,0,comm))
        failure("MPI_Gather");

/* Take the maximum over cpus or nodes, as appropriate. */

    reset_analysis(&anal_datum);
    for (i = 0; i < nodes; ++i) reset_analysis(&anal_nodes[i]);
    if (procid == 0) {
        for (i = 0; i < procs; ++i) {
            n = getnode(i);
            if (anal_cpus[i].callspercpu > anal_datum.callspercpu)
                anal_datum.callspercpu = anal_cpus[i].callspercpu;
            if (anal_cpus[i].copiespercpu > anal_datum.copiespercpu)
                anal_datum.copiespercpu = anal_cpus[i].copiespercpu;
            if (anal_cpus[i].selfpercpu > anal_datum.selfpercpu)
                anal_datum.selfpercpu = anal_cpus[i].selfpercpu;
            if (anal_cpus[i].otherpercpu > anal_datum.otherpercpu)
                anal_datum.otherpercpu = anal_cpus[i].otherpercpu;
            anal_nodes[n].callspernode += anal_cpus[i].callspernode;
            anal_nodes[n].copiespernode += anal_cpus[i].copiespernode;
            anal_nodes[n].intpernode += anal_cpus[i].intpernode;
            anal_nodes[n].extpernode += anal_cpus[i].extpernode;
            anal_datum.external += anal_cpus[i].external;
        }
        for (i = 0; i < nodes; ++i) {
            if (anal_nodes[i].callspernode > anal_datum.callspernode)
                anal_datum.callspernode = anal_nodes[i].callspernode;
            if (anal_nodes[i].copiespernode > anal_datum.copiespernode)
                anal_datum.copiespernode = anal_nodes[i].copiespernode;
            if (anal_nodes[i].intpernode > anal_datum.intpernode)
                anal_datum.intpernode = anal_nodes[i].intpernode;
            if (anal_nodes[i].extpernode > anal_datum.extpernode)
                anal_datum.extpernode = anal_nodes[i].extpernode;
        }
    }

/* Sum into the main data and reset the temporary counts. */

    analysis.callspercpu += anal_datum.callspercpu;
    analysis.copiespercpu += anal_datum.copiespercpu;
    analysis.selfpercpu += anal_datum.selfpercpu;
    analysis.otherpercpu += anal_datum.otherpercpu;
    analysis.callspernode += anal_datum.callspernode;
    analysis.copiespernode += anal_datum.copiespernode;
    analysis.intpernode += anal_datum.intpernode;
    analysis.extpernode += anal_datum.extpernode;
    analysis.external += anal_datum.external;
    if (trace > 1 && procid == 0)
        fprintf(stderr,
            "analysis(2):  %s %s %s %d %d    %d %d    %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f\n",
            global_buffering.tag,global_method.tag,global_grouping.tag,
            worldprocs,worldid,procs,procid,analysis.passes,
            analysis.callspercpu,analysis.copiespercpu,analysis.selfpercpu,
            analysis.otherpercpu,analysis.callspernode,
            analysis.copiespernode,analysis.intpernode,analysis.extpernode,
            analysis.external);
    reset_analysis(&anal_datum);
}



void pairwise (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Ssend and MPI_Recv in handshake mode for a combination
ping-pong/bisection test. */

    int i, k, n;
    MPI_Status status;
    int (*send) (void *, int, MPI_Datatype, int, int, MPI_Comm) =
        global_buffering.send;

    if (trace)
        fprintf(stderr,
            "pairwise:  %s %s %s %d %d    %d %d %ld\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,
            (long)size);

    for (n = 0; n < repeat; ++n) {
        if ((procs&0x1) != 0 && procid >= procs-1)
            ;
        else if ((procid&0x1) == 0) {
            k = procid+1;
            if (send(sendbuf,size,MPI_DOUBLE,k,procs*procid+k,comm))
                failure(global_buffering.send_tag);
            i = 0;
            if (MPI_Recv(recvbuf,size,MPI_DOUBLE,k,procs*k+procid,
                        comm,&status) ||
                    (i = 1, 0) || status.MPI_SOURCE != k ||
                    status.MPI_TAG != procs*k+procid) {
                if (i)
                    fprintf(stderr,"Mismatch:  %d != %d or %d != %d\n",
                        status.MPI_SOURCE,k,status.MPI_TAG,procs*k+procid);
                failure("MPI_Recv");
            }
            if (initial) update_analysis(procs,procid,k,0,2,2);
        } else {
            k = procid-1;
            i = 0;
            if (MPI_Recv(recvbuf,size,MPI_DOUBLE,k,procs*k+procid,
                        comm,&status) ||
                    (i = 1, 0) || status.MPI_SOURCE != k ||
                    status.MPI_TAG != procs*k+procid) {
                if (i)
                    fprintf(stderr,"Mismatch:  %d != %d or %d != %d\n",
                        status.MPI_SOURCE,k,status.MPI_TAG,procs*k+procid);
                failure("MPI_Recv");
            }
            if (send(sendbuf,size,MPI_DOUBLE,k,procs*procid+k,comm))
                failure(global_buffering.send_tag);
            if (initial) update_analysis(procs,procid,k,0,2,2);
        }
        if (initial) {
            update_analysis(procs,procid,-1,1,0,0);
            gather_analysis(comm,procs,procid);
        }
    }
}



void ipairwise (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Issend and MPI_Irecv in duplex mode for a combination
ping-pong/bisection test. */

    int i, k, n;
    MPI_Status status[2];
    MPI_Request request[2];

    if (trace)
        fprintf(stderr,
            "ipairwise:  %s %s %s %d %d    %d %d %ld\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size);
    for (n = 0; n < repeat; ++n) {
        if ((procs&0x1) != 0 && procid >= procs-1)
            ;
        else {
            k = procid^0x1;
            if (MPI_Irecv(recvbuf,size,MPI_DOUBLE,k,procs*k+procid,
                    comm,&request[1]))
                failure("MPI_Recv");
            if (initial) update_analysis(procs,procid,k,0,1,1);
            if (global_buffering.isend(sendbuf,size,MPI_DOUBLE,
                    k,procs*procid+k,comm,&request[0]))
                failure(global_buffering.isend_tag);
            if (initial) update_analysis(procs,procid,k,0,1,1);
            i = 0;
            if (MPI_Waitall(2,request,status) ||
                    (i = 1, 0) || status[1].MPI_SOURCE != k ||
                    status[1].MPI_TAG != procs*k+procid) {
                if (i)
                    fprintf(stderr,"Mismatch:  %d != %d or %d != %d\n",
                        status[1].MPI_SOURCE,k,
                        status[1].MPI_TAG,procs*k+procid);
                failure("MPI_Waitall");
            }
            if (initial) update_analysis(procs,procid,k,0,1,0);
        }
        if (initial) gather_analysis(comm,procs,procid);
    }
}



void alltoall (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Alltoall. */

    int n;

    if (trace)
        fprintf(stderr,
            "alltoall:  %s %s %s %d %d    %d %d %ld\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size);
    for (n = 0; n < repeat; ++n) {
        if (MPI_Alltoall(sendbuf,size,MPI_DOUBLE,recvbuf,size,MPI_DOUBLE,comm))
            failure("MPI_Alltoall");
        if (initial) {
            update_analysis(procs,procid,procid,0,0,2);
            update_analysis(procs,procid,-1,0,1,2);
            gather_analysis(comm,procs,procid);
        }
    }
}



void gather (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Gather.  Note that the analysis figures are all zero because
it isn't possible to give them meaningful values. */

    int i, n;

    if (trace)
        fprintf(stderr,
            "gather:  %s %s %s %d %d    %d %d %ld\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size);
    for (n = 0; n < repeat; ++n)
        for (i = 0; i < procs; ++i) {
            if (MPI_Gather(sendbuf+i*size,size,MPI_DOUBLE,
                    recvbuf,size,MPI_DOUBLE,i,comm))
                failure("MPI_Gather");
        }
}



void scatter (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Scatter.  Note that the analysis figures are all zero because
it isn't possible to give them meaningful values. */

    int i, n;

    if (trace)
        fprintf(stderr,
            "scatter:  %s %s %s %d %d    %d %d %ld\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size);
    for (n = 0; n < repeat; ++n)
        for (i = 0; i < procs; ++i) {
            if (MPI_Scatter(sendbuf,size,MPI_DOUBLE,
                    recvbuf+i*size,size,MPI_DOUBLE,i,comm))
                failure("MPI_Scatter");
        }
}



void handshake (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Ssend and MPI_Recv in handshake parallel mode. */

    int i, j, k, l, n;
    MPI_Status status;

    if (trace)
        fprintf(stderr,
            "handshake:  %s %s %s %d %d    %d %d %ld    %d %p %p\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size,
            simplexmapsize,simplexsendmap,simplexrecvmap);
    for (n = 0; n < repeat; ++n) {
        for (i = 0; i < simplexmapsize; ++i) {
            k = simplexsendmap[i*procs+procid];
            l = simplexrecvmap[i*procs+procid];
            if (trace > 1)
                fprintf(stderr,
                    "transfer:  %s %s %s %d %d    %d %d    %d %d %d\n",
                    global_buffering.tag,global_method.tag,global_grouping.tag,
                    worldprocs,worldid,procs,procid,i,k,l);
            if (k == procid) {
                memcpy(recvbuf+procid*size,sendbuf+procid*size,
                    size*sizeof(double));
                if (initial) update_analysis(procs,procid,procid,0,0,2);
            } else if (k >= 0) {
                if (global_buffering.send(sendbuf+k*size,size,
                        MPI_DOUBLE,k,procs*procid+k,comm))
                    failure(global_buffering.send_tag);
                if (initial) update_analysis(procs,procid,k,0,1,1);
            } else if (l >= 0) {
                j = 0;
                if (MPI_Recv(recvbuf+l*size,size,MPI_DOUBLE,
                            l,procs*l+procid,comm,&status) ||
                        (j = 1, 0) || status.MPI_SOURCE != l ||
                        status.MPI_TAG != procs*l+procid) {
                    if (j)
                        fprintf(stderr,"Mismatch:  %d != %d or %d != %d\n",
                            status.MPI_SOURCE,l,status.MPI_TAG,procs*l+procid);
                    failure("MPI_Recv");
                }
                if (initial) update_analysis(procs,procid,l,0,1,1);
            }
            if (initial) gather_analysis(comm,procs,procid);
        }
    }
}



void duplex (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Issend and MPI_Irecv in duplex parallel mode. */

    int i, j, k, l, n;
    MPI_Status status[2];
    MPI_Request request[2];

    if (trace)
        fprintf(stderr,
            "duplex:  %s %s %s %d %d    %d %d %ld    %d %p\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size,
            duplexmapsize,duplexmap);
    for (n = 0; n < repeat; ++n) {
        for (i = 0; i < duplexmapsize; ++i) {
            k = duplexmap[i*procs+procid];
            if (k == procid) {
                memcpy(recvbuf+procid*size,sendbuf+procid*size,
                    size*sizeof(double));
                if (initial) update_analysis(procs,procid,procid,0,0,2);
            } else if (k >= 0) {
                if (trace > 1)
                    fprintf(stderr,
                        "transfer:  %s %s %s %d %d    %d %d    %d %d\n",
                        global_method.tag,global_grouping.tag,
                        global_buffering.tag,worldprocs,worldid,procs,
                        procid,i,k);
                l = procs*k+procid;
                if (MPI_Irecv(recvbuf+k*size,size,MPI_DOUBLE,
                        k,l,comm,&request[1]))
                    failure("MPI_Irecv");
                if (initial) update_analysis(procs,procid,k,0,1,1);
                if (global_buffering.isend(sendbuf+k*size,size,
                        MPI_DOUBLE,k,procs*procid+k,comm,&request[0]))
                    failure(global_buffering.isend_tag);
                if (initial) update_analysis(procs,procid,k,0,1,1);
                j = 0;
                if (MPI_Waitall(2,request,status) ||
                        (j = 1, 0) || status[1].MPI_SOURCE != k ||
                        status[1].MPI_TAG != l) {
                    if (j)
                        fprintf(stderr,"Mismatch:  %d != %d or %d != %d\n",
                            status[1].MPI_SOURCE,k,status[1].MPI_TAG,l);
                    failure("MPI_Waitall");
                }
                if (initial) update_analysis(procs,procid,k,0,1,0);
            }
            if (initial) gather_analysis(comm,procs,procid);
        }
    }
}



void flipflop (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Sendrecv in duplex parallel mode. */

    int i, j, k, l, n;
    MPI_Status status;

    if (trace)
        fprintf(stderr,
            "flipflop:  %s %s %s %d %d    %d %d %ld    %d %p\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size,
            duplexmapsize,duplexmap);
    for (n = 0; n < repeat; ++n) {
        for (i = 0; i < duplexmapsize; ++i) {
            k = duplexmap[i*procs+procid];
            if (k == procid) {
                memcpy(recvbuf+procid*size,sendbuf+procid*size,
                    size*sizeof(double));
                if (initial) update_analysis(procs,procid,procid,0,0,2);
            } else if (k >= 0) {
                if (trace > 1)
                    fprintf(stderr,
                        "transfer:  %s %s %s %d %d    %d %d    %d %d\n",
                        global_method.tag,global_grouping.tag,
                        global_buffering.tag,worldprocs,worldid,procs,
                        procid,i,k);
                l = procs*k+procid;
                j = 0;
                if (MPI_Sendrecv(sendbuf+k*size,size,MPI_DOUBLE,k,
                        procs*procid+k,recvbuf+k*size,size,MPI_DOUBLE,
                        k,l,comm,&status) || (j = 1, 0) ||
                        status.MPI_SOURCE != k || status.MPI_TAG != l) {
                    if (j)
                        fprintf(stderr,"Mismatch:  %d != %d or %d != %d\n",
                            status.MPI_SOURCE,k,status.MPI_TAG,l);
                    failure("MPI_Sendrecv");
                }
                if (initial) update_analysis(procs,procid,k,0,1,2);
            }
            if (initial) gather_analysis(comm,procs,procid);
        }
    }
}



void roundrobin (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Sendrecv in round-robin parallel mode. */

    int i, i1, j, k, l, m, n;
    MPI_Status status;

    if (trace)
        fprintf(stderr,
            "roundrobin:  %s %s %s %d %d    %d %d %ld    %d %p\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size,
            duplexmapsize,duplexmap);

/* Find the index of the current processor.  The time spent here is
negligible. */

    i1 = -1;
    for (i = 0; i < procs; ++i) if (sequence[i] == procid) i1 = i;
    if (i1 < 0) failure("Consistency failing in roundrobin");

/* Now do the real work. */

    for (n = 0; n < repeat; ++n) {
        for (i = 1; i < procs; ++i) {
            k = sequence[(i1-i+procs)%procs];
            l = sequence[(i1+i)%procs];
            if (trace > 1)
                fprintf(stderr,
                    "transfer2:  %s %s %s %d %d    %d %d    %d %d %d\n",
                    global_method.tag,global_grouping.tag,global_buffering.tag,
                    worldprocs,worldid,procs,procid,i,k,l);
            m = procs*k+procid;
            if (MPI_Sendrecv(sendbuf+l*size,size,MPI_DOUBLE,l,
                    procs*procid+l,recvbuf+k*size,size,MPI_DOUBLE,
                    k,m,comm,&status) || (j = 1, 0) ||
                    status.MPI_SOURCE != k || status.MPI_TAG != m) {
                if (j)
                    fprintf(stderr,"Mismatch:  %d != %d or %d != %d\n",
                        status.MPI_SOURCE,k,status.MPI_TAG,m);
                failure("MPI_Sendrecv");
            }
            if (initial) {
                update_analysis(procs,procid,k,0,1,2);
                gather_analysis(comm,procs,procid);
            }
        }
        memcpy(recvbuf+procid*size,sendbuf+procid*size,size*sizeof(double));
        if (initial) {
            update_analysis(procs,procid,procid,0,0,2);
            if (initial) gather_analysis(comm,procs,procid);
        }
    }
}



void multiplex (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Issend and MPI_Irecv in multiplex parallel mode.  Note
that the order is always randomised, with remote transfers first.
It would be possible to play with variations, but doesn't seem worth
the hassle.  Note that calling makepairs here and corrupting mappairs is
rather disgusting scoping. */

    int count, i, k, n;

    if (trace)
        fprintf(stderr,"multiplex(1):  %s %s %s %d %d    %d %d %ld\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size);

/* Select only the relevant transfers.  The time spent here is assumed
negligible; it may not be quite true, but should be close enough. */

    count = procs*(procs-1);
    makepairs(procs,0,1,1);
    k = 0;
    for (i = 0; i < count; ++i)
        if (mappairs[i][0] == procid || mappairs[i][1] == procid) {
            mappairs[k][0] = mappairs[i][0];
            mappairs[k][1] = mappairs[i][1];
            ++k;
        }
    if (k != 2*procs-2) failure("multiplex");
    if (trace) {
        fprintf(stderr,"multiplex(2):  %s %s %s %d %d  ",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid);
        for (i = 0; i < 2*(procs-1); ++i)
            fprintf(stderr,"  %d,%d",mappairs[i][0],mappairs[i][1]);
        fprintf(stderr,"\n");
    }

/* Now do the real work. */

    for (n = 0; n < repeat; ++n) {
        for (i = 0; i < 2*procs-2; ++i)
            if (mappairs[i][0] == procid) {
                k = mappairs[i][1];
                if (global_buffering.isend(sendbuf+k*size,size,MPI_DOUBLE,
                        k,procs*procid+k,comm,&global_request[i]))
                    failure(global_buffering.isend_tag);
                if (initial) update_analysis(procs,procid,k,0,1,1);
            } else {
                k = mappairs[i][0];
                if (MPI_Irecv(recvbuf+k*size,size,MPI_DOUBLE,
                        k,procs*k+procid,comm,&global_request[i]))
                    failure("MPI_Irecv");
                if (initial) update_analysis(procs,procid,k,0,1,1);
            }
        memcpy(recvbuf+procid*size,sendbuf+procid*size,size*sizeof(double));
        if (initial) update_analysis(procs,procid,procid,0,0,2);
        if (MPI_Waitall(2*procs-2,global_request,global_status))
            failure("MPI_Waitall");
        if (initial) {
            update_analysis(procs,procid,-1,0,1,0);
            gather_analysis(comm,procs,procid);
        }
        for (i = 0; i < 2*procs-2; ++i) {
            k = mappairs[i][0];
            if (mappairs[i][0] != procid &&
                    (global_status[i].MPI_SOURCE != k ||
                        global_status[i].MPI_TAG != procs*k+procid)) {
                fprintf(stderr,"Mismatch:  %d != %d or %d != %d or %d != %d\n",
                        mappairs[i][0],procid,global_status[i].MPI_SOURCE,
                        k,global_status[i].MPI_TAG,procs*k+procid);
                failure("MPI request status");
            }
        }
    }
}



void broadcast (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Bcast.  Note that the analysis figures are all zero because
it isn't possible to give them meaningful values. */

    int i, n;

    if (trace)
        fprintf(stderr,"broadcast:  %s %s %s %d %d    %d %d %ld\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size);
    for (i = 0; i < size; ++i)
        recvbuf[procid*size+i] = sendbuf[procid*size+i];
    for (n = 0; n < repeat; ++n)
        for (i = 0; i < procs; ++i)
            if (MPI_Bcast(recvbuf+i*size,size,MPI_DOUBLE,i,comm))
                failure("MPI_Bcast");
}



void tree (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Ssend in a tree structure.  Distribute to nodes first, and
then within nodes, using a predefined map.  Note that the analysis
figures are all zero because it isn't possible to give them meaningful
values. */

    int i, j, k, l, m, n;
    MPI_Status status;
    double *ptr;

    if (trace)
        fprintf(stderr,"tree:  %s %s %s %d %d    %d %d %ld\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size);
    for (n = 0; n < repeat; ++n) {
        for (m = 0; m < procs; ++m) {
            ptr = sendbuf;
            for (i = 0; i < bcastmapsize; ++i) {
                j = i*procs+(procs+procid-m)%procs;
                k = bcastsendmap[j];
                l = bcastrecvmap[j];
                if (trace > 1)
                    fprintf(stderr,
                        "transfer:  %s %s %s %d %d    %d %d    %d %d %d\n",
                        global_buffering.tag,global_method.tag,
                        global_grouping.tag, worldprocs,worldid,procs,
                        procid,i,k,l);
                if (k >= 0) {
                    k = (k+m)%procs;
                    if (global_buffering.send(ptr+m*size,size,
                            MPI_DOUBLE,k,procs*m+k,comm))
                        failure(global_buffering.send_tag);
                } else if (l >= 0) {
                    l = (l+m)%procs;
                    j = 0;
                    if (MPI_Recv(recvbuf+m*size,size,MPI_DOUBLE,
                                l,procs*m+procid,comm,&status) ||
                            (j = 1, 0) || status.MPI_SOURCE != l ||
                            status.MPI_TAG != procs*m+procid) {
                        if (j)
                            fprintf(stderr,"Mismatch:  %d != %d or %d != %d\n",
                                status.MPI_SOURCE,l,status.MPI_TAG,
                                procs*m+procid);
                        failure("MPI_Recv");
                    }
                }
                if (procid != m) ptr = recvbuf;
                if (i == bcastmapsize-1)
                    memcpy(recvbuf+procid*size,sendbuf+procid*size,
                        size*sizeof(double));
                if (MPI_Barrier(comm)) failure("MPI_Comm_barrier");
            }
        }
    }
}



void widetree (double sendbuf[], double recvbuf[], size_t size,
        int procs, int procid, MPI_Comm comm, int repeat, int initial) {

/* Test MPI_Issend in a wide tree structure.  Distribute to nodes first,
and then within nodes, using a predefined map.  Note that the analysis
figures are all zero because it isn't possible to give them meaningful
values. */

    int i, j, k, l, m, n, n1;
    MPI_Status status;
    double *ptr;

    if (trace)
        fprintf(stderr,"widetree:  %s %s %s %d %d    %d %d %ld\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,procs,procid,(long)size);
    for (n = 0; n < repeat; ++n) {
        for (m = 0; m < procs; ++m) {
            ptr = sendbuf;
            n1 = 0;
            for (i = 0; i < bcastmapsize; ++i) {
                j = i*procs+(procs+procid-m)%procs;
                k = bcastsendmap[j];
                l = bcastrecvmap[j];
                if (trace > 1)
                    fprintf(stderr,
                        "transfer:  %s %s %s %d %d    %d %d    %d %d %d\n",
                        global_buffering.tag,global_method.tag,
                        global_grouping.tag,worldprocs,worldid,procs,
                        procid,i,k,l);
                if (k >= 0) {
                    k = (k+m)%procs;
                    if (global_buffering.isend(ptr+m*size,size,MPI_DOUBLE,
                            k,procs*m+k,comm,&global_request[n1++]))
                        failure(global_buffering.isend_tag);
                } else if (l >= 0) {
                    l = (l+m)%procs;
                    j = 0;
                    if (MPI_Recv(recvbuf+m*size,size,MPI_DOUBLE,
                                l,procs*m+procid,comm,&status) ||
                            (j = 1, 0) || status.MPI_SOURCE != l ||
                            status.MPI_TAG != procs*m+procid) {
                        if (j)
                            fprintf(stderr,"Mismatch:  %d != %d or %d != %d\n",
                                status.MPI_SOURCE,l,
                                status.MPI_TAG,procs*m+procid);
                        failure("MPI_Recv");
                    }
                }
                if (procid != m) ptr = recvbuf;
            }
            memcpy(recvbuf+procid*size,sendbuf+procid*size,size*sizeof(double));

/* In this case, we don't wait until the end; this looks unchecked, but
isn't any more than any other code. */

            if (n1 > 0 && MPI_Waitall(n1,global_request,global_status))
                failure("MPI_Waitall");
            if (MPI_Barrier(comm)) failure("MPI_Comm_barrier");
        }
    }
}



/* Define some action codes, for readability. */

enum output_action {Read_Once, Read_Many, Write_Once, Write_Ending};
   


void handle_output (int action, int tag, char *iobuf) {

/* Pipe all output through worldid 0, to avoid the various 'features' of
I/O in MPI implementations.  If worldid 0 is part of the current
communicator, each message (including the end of iteration message) is
handled on its own; if it is not, then it loops until it sees the end of
iteration message. */

    MPI_Status status;
    int k, count;

    if (trace)
        fprintf(stderr,
            "handle_output:  %s %s %s %d %d    %d %d\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,action,tag);

/* The complicated part is receiving the data. */

    if (action == Read_Once || action == Read_Many) {
        do {
            count = k = 0;
            if (MPI_Recv(iobuf,256,MPI_CHAR,MPI_ANY_SOURCE,
                        MPI_ANY_TAG,MPI_COMM_WORLD,&status) ||
                    (k = 1, 0) || status.MPI_SOURCE != cpulist[0] ||
                    status.MPI_TAG != tag ||
                    MPI_Get_count(&status,MPI_CHAR,&count) ||
                    count != 256) {
                if (k)
                    fprintf(stderr,
                        "Mismatch:  %d != %d or %d != %d or %d != %d\n",
                        status.MPI_SOURCE,cpulist[0],status.MPI_TAG,tag,
                        count,256);
                failure("MPI_Recv");
            }
            ++tag;
            if (iobuf[0] == '\0')
                return;
            else
                printf("%s",iobuf);
        } while (action == Read_Many);

/* Sending it is much easier. */

    } else if (action == Write_Once || action == Write_Ending) {
        if (action == Write_Ending) iobuf[0] = '\0';
        if (worldid == 0)
            printf("%s",iobuf);
        else if (MPI_Send(iobuf,256,MPI_CHAR,0,tag,MPI_COMM_WORLD))
                failure("MPI_Send");
    } else 
        failure("Unknown action in handle_output");
}



void iterate (double sendbuf[], double recvbuf[], MPI_Comm comm) {

/* Iterate down the sizes, repeating often enough to get some decent
data. */

    int procs, procid, initial, tag;
    size_t repeat, size, i, j, k, n;
    double decimal, starting, used, sum;
    char iobuf[256];

/* Print some diagnostics. */

    if (MPI_Comm_size(comm,&procs)) failure("MPI_Comm_size");
    if (MPI_Comm_rank(comm,&procid)) failure("MPI_Comm_rank");
    decimal = 1.0;
    while (decimal < 10.0*procs*(double)startsize) decimal *= 10.0;
    tag = 123+procs*(procs-1);
    if (trace) 
        fprintf(stderr,"iterate:  %s %s %s %d %d    %ld    %d %d\n",
            global_method.tag,global_grouping.tag,global_buffering.tag,
            worldprocs,worldid,(long)startsize,procs,procid);

/* Initialise the source buffer and iterate down the sizes. */

    for (i = 0; i < procs*startsize; ++i)
        sendbuf[i] = decimal*(procid+1)+i+1;
    size = startsize;
    starting = MPI_Wtime();
    do {
        if (MPI_Barrier(comm)) failure("MPI_Comm_barrier");
        initial = 1;
        repeat = 1;

/* Run the test, increasing the repeat as necessary, and ignoring the
first call. */

        while (1) {
            for (i = 0; i < procs*size; ++i) recvbuf[i] = 0.0;
            if (initial) {
                reset_analysis(&analysis);
                reset_analysis(&anal_datum);
            }
            if (MPI_Barrier(comm)) failure("MPI_Comm_barrier");
            used = MPI_Wtime();
            global_method.method(sendbuf,recvbuf,size,procs,procid,
                comm,repeat,initial);
            if (MPI_Barrier(comm)) failure("MPI_Comm_barrier");
            used = MPI_Wtime()-used;
            if (MPI_Allreduce(&used,&sum,1,MPI_DOUBLE,MPI_SUM,comm))
                failure("MPI_Allreduce");

/* Check that the last all-to-all or broadcast result is correct. */

            if (global_method.alltoall) {
                for (i = 0; i < procs; ++i) {
                    n = (global_method.alltoall > 1 ?
                            decimal*(i+1)+i*size :
                            decimal*(i+1)+procid*size);
                    for (j = 0; j < size; ++j)
                        if (recvbuf[size*i+j] != n+j+1) {
                            fprintf(stderr,
"error:  %s %s %s %d %d    %d %d    %ld    %ld %ld    %.1f %.1f\n",
                                global_method.tag,global_grouping.tag,
                                global_buffering.tag,worldprocs,worldid,
                               procs,procid,(long)size,(long)i,(long)j,
                                (double)(n+j+1),recvbuf[size*i+j]);
                            if (trace) {
                                for (k = 0; k < procs*size; ++k)
                                    fprintf(stderr,"%.0f%c",recvbuf[k],
                                        (k%10 == 9 ? '\n' : ' '));
                                fprintf(stderr,"\n");
                            }
                            failure("Consistency check");
                        }
                }

/* Check that the ping-pong result is correct. */

            } else if ((procs&0x1) == 0 || procid < procs-1) {
                 j = procid^0x1;
                 for (i = 0; i < size; ++i)
                    if (recvbuf[i] != decimal*(j+1)+i+1) {
                        fprintf(stderr,
"error:  %s %s %s %d %d    %d %d    %ld    %ld    %.1f %.1f\n",
                            global_method.tag,global_grouping.tag,
                            global_buffering.tag,worldprocs,worldid,
                            procs,procid,(long)size,(long)i,
                                (double)(decimal*(j+1)+i+1),recvbuf[i]);
                            if (trace > 1) {
                                for (k = 0; k < size; ++k)
                                    fprintf(stderr,"%.0f%c",recvbuf[k],
                                        (k%10 == 9 ? '\n' : ' '));
                                fprintf(stderr,"\n");
                            }
                            failure("Consistency check");
                        }
            } 

/* Repeat the test until we have some decent data.  The kludge with WASTE
is for systems with asynchronous initialisation. */

            used = sum/procs;
            if (veryfirst && MPI_Wtime() < starting+WASTE*(1.0+testtime))
                ;
            else if (! initial && used > testtime)
                break;
            veryfirst = initial = 0;
            k = ceil(repeat*1.1*testtime/(used+0.01*testtime));
            if (repeat < k) repeat = k;
            if (trace) 
                fprintf(stderr,
                    "repeat:  %s %s %s %d %d    %d %ld    %.3f %ld\n",
                    global_method.tag,global_grouping.tag,
                    global_buffering.tag,worldprocs,worldid,procs,
                    (long)size,used,(long)repeat);
        };

/* Now print the results from the first node and repeat. */

        if (procid == 0) {
            n = sprintf(iobuf,
                "%s %s %s   %d %d   %d %ld   %ld %.3f  %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f\n",
                global_method.tag,global_grouping.tag,
                global_buffering.tag,worldprocs,localprocs,procs,
                (long)(size*sizeof(double)),(long)repeat,used,
                analysis.passes,analysis.callspercpu,analysis.copiespercpu,
                analysis.selfpercpu,analysis.otherpercpu,
                analysis.callspernode,analysis.copiespernode,
                analysis.intpernode,analysis.extpernode,analysis.external);
            if (n >= 256)
                failure("Internal error - buffer overflow in iterate");
            handle_output(Write_Once,tag,iobuf);
        } else if (worldid == 0)
            handle_output(Read_Once,tag,iobuf);
        ++tag;
        k = size*sizefactor+0.5;
        size = (k >= size ? k-1 : k);
    } while (size >= minsize);
    if (procid == 0 && worldid != 0)
        handle_output(Write_Ending,tag,iobuf);
    else if (procid != 0 && worldid == 0)
        handle_output(Read_Once,tag,iobuf);
}



void subdivide (double sendbuf[], double recvbuf[]) {

/* Subdivide down the number of processes, in a variety of bizarre ways. */

    int procs, i, k;
    MPI_Comm comm;
    MPI_Group group, worldgroup;
    char iobuf[256];

/* Start subdividing. */

    if (MPI_Comm_group(MPI_COMM_WORLD,&worldgroup))
        failure("MPI_Comm_group");
    procs = startprocs;
    do {
        choosemap(procs,0);
        choosemap(procs,1);
        choosemap(procs,2);
        choosecast(procs);
        if (trace) {
            displaymap(stderr,0);
            displaymap(stderr,1);
            displaymap(stderr,2);
            displaymap(stderr,3);
        }

/* Set up the restricted communicator. */

        if (strcmp(global_grouping.tag,"contiguous") == 0)
            for (i = 0; i < procs; ++i) cpulist[i] = i;
        else if (strcmp(global_grouping.tag,"bisected") == 0)
            for (i = 0; i < procs; ++i)
                cpulist[i] = i/2+((i&0x1) == 0 ? 0 : (worldprocs+1)/2);
        else if (strcmp(global_grouping.tag,"distributed") == 0)
            for (i = 0; i < procs; ++i) cpulist[i] = reverse(i);
        else if (strcmp(global_grouping.tag,"shuffled") == 0) {
            k = 123;
            for (i = 0; i < procs; ++i)
                cpulist[i] = k = shuffle(k,worldprocs);
        } else
            failure("Internal error - bad grouping in subdivide");
        if (trace) {
            fprintf(stderr,"subdivide:  %s %s %s %d %d    %d\n",
                global_method.tag,global_grouping.tag,
                global_buffering.tag,worldprocs,worldid,procs);
            fprintf(stderr,"%d:  ",worldid);
            for (i = 0; i < procs; ++i) fprintf(stderr," %d",cpulist[i]);
            fprintf(stderr,"\n");
        }
        if (MPI_Group_incl(worldgroup,procs,cpulist,&group))
            failure("MPI_Group_incl");
        if (MPI_Comm_create(MPI_COMM_WORLD,group,&comm))
            failure("MPI_Comm_create");

/* If appropriate, do the operation, and then move onto the next stage.
The test is done this way because the MPI standard is not clear how you
test for MPI_COMM_NULL.  There is also an ambiguity in whether
MPI_Comm_free should be symmetric with MPI_Comm_create or not, but IBM
thinks not. */

        k = 0;
        for (i = 0; i < procs; ++i) if (worldid == cpulist[i]) k = 1;
        if (k) {
            iterate(sendbuf,recvbuf,comm);
            if (MPI_Comm_free(&comm)) failure("MPI_Comm_free");
        } else if (worldid == 0)
            handle_output(Read_Many,123+procs*(procs-1),iobuf);
        if (MPI_Group_free(&group)) failure("MPI_Group_free");
        k = procs*procfactor+0.5;
        procs = (k >= procs ? k-1 : k);
    } while (procs >= minprocs);
}



int main (int argc, char *argv[]) {
    double *sendbuf, *recvbuf, *attachbuf, decimal;
    static const char *mapmethods[] = {"passes","total","peak","overall"},
       *method = NULL, *grouping = NULL, *buffering = NULL;
    size_t attachsize, z;
    int alloc = 0, i, i1, j, j1, k, k1, l;
    char c;
    void *q;

/* Print the syntax if no arguments. */

    setvbuf(stdout,NULL,_IOLBF,256);
    setvbuf(stderr,NULL,_IOLBF,256);
    if (argc <= 1) {
        fprintf(stderr,
            "Syntax: mpi_timer [ -trace <012> ] -method <piagshdfrmbtw>\n");
        fprintf(stderr,"    [ -grouping <wbcds> ] [ -buffering <dbs> ]\n");
        fprintf(stderr,
            "    [ - allocate ] [ -time time ] [ -procs number ]\n");
        fprintf(stderr,
            "    [ -minprocs number ] [ -procfactor factor ]\n");
        fprintf(stderr,
            "    [ -localprocs number [ technique ] ]\n");
        fprintf(stderr,
            "    -size size [ -minsize size ] [ -sizefactor factor ]\n");
        exit(EXIT_FAILURE);
    }

/* Initalise MPI and find out the environment.  Note that the restriction
on the size of MPI_COMM_WORLD is relaxable, but I can't be bothered at
present. */

    if (MPI_Init(&argc,&argv)) {
         fprintf(stderr,"Failure: MPI_Init\n");
        exit(EXIT_FAILURE);
    }
    if (MPI_Comm_size(MPI_COMM_WORLD,&worldprocs))
        failure("MPI_Comm_size");
    if (MPI_Comm_rank(MPI_COMM_WORLD,&worldid))
        failure("MPI_Comm_rank");
    if (worldprocs < 2 || worldprocs > 65536)
        failure("Incorrect number of MPI processes");
    if (worldprocs > sqrt((double)(INT_MAX/MAXPASSES)))
        failure("Too many MPI processes for INT_MAX");
    power2procs = 1;
    i = worldprocs-1;
    while (i > 0) {
        power2procs <<= 1;
        i >>= 1;
    }

/* Check and decode the arguments. */

    for (i = 1; i < argc; i += 2) {
        k = 0;
        if (! alloc && matchstring(argv[i],"-allocate")) {
            alloc = 1;
            --i;
        } else if (i >= argc-1) {
            fprintf(stderr,"At shell argument %d\n",i);
            failure("Missing argument value");
        } else if (trace < 0 && matchstring(argv[i],"-trace") &&
                (k = 1, 1) && sscanf(argv[i+1],"%d%c",&trace,&c) == 1 &&
                trace >= 0 && trace <= 2)
            ;
        else if (method == NULL && matchstring(argv[i],"-method") &&
                (k = 1, 1) &&
                (method = matcharg(argv[i+1],methods,COUNT(methods),
                    sizeof(method_type),method_all)) != NULL)
            ;
        else if (grouping == NULL && matchstring(argv[i],"-grouping") &&
                (k = 1, 1) &&
                (grouping = matcharg(argv[i+1],groupings,
                    COUNT(groupings),sizeof(grouping_type),
                    grouping_all)) != NULL)
            ;
        else if (buffering == NULL &&
                matchstring(argv[i],"-buffering") && (k = 1) != 0 &&
                (buffering = matcharg(argv[i+1],bufferings,
                    COUNT(bufferings),sizeof(buffering_type),
                    buffering_all)) != NULL)
            ;
        else if (testtime < 0 && matchstring(argv[i],"-time") &&
                (k = 1, 1) &&
                sscanf(argv[i+1],"%lf%c",&testtime,&c) == 1 &&
                testtime > 0.000999999 && testtime <= 300.0)
            ;
        else if (startsize == 0 && matchstring(argv[i],"-size") &&
                (k = 1, 1) && (startsize = readsize(argv[i+1])) != 0)
            ;
        else if (minsize == 0 && matchstring(argv[i],"-minsize") &&
                (k = 1, 1) && (minsize = readsize(argv[i+1])) != 0)
            ;
        else if (sizefactor < 0 &&
                    matchstring(argv[i],"-sizefactor") &&
                (k = 1, 1) &&
                sscanf(argv[i+1],"%lf%c",&sizefactor,&c) == 1 &&
                sizefactor > 0.0 && sizefactor < 1.0)
            ;
        else if (startprocs < 0 && matchstring(argv[i],"-procs") &&
                (k = 1, 1) &&
                sscanf(argv[i+1],"%d%c",&startprocs,&c) == 1 &&
                startprocs >= 2 && startprocs <= 16384)
            ;
        else if (minprocs < 0 && matchstring(argv[i],"-minprocs") &&
                (k = 1, 1) &&
                sscanf(argv[i+1],"%d%c",&minprocs,&c) == 1 &&
                minprocs >= 2 && minprocs <= 16384)
            ;
        else if (procfactor < 0 &&
                    matchstring(argv[i],"-procfactor") &&
                (k = 1, 1) &&
                sscanf(argv[i+1],"%lf%c",&procfactor,&c) == 1 &&
                procfactor > 0.0 && procfactor < 1.0)
            ;
        else if (localprocs == 0 && matchstring(argv[i],"-localprocs") &&
                (k = 1, 1) && sscanf(argv[i+1],"%d%c",&localprocs,&c) == 1 &&
                abs(localprocs) >= 2 && abs(localprocs) <= 4096) {
            for (j = 0; j < COUNT(mapmethods); ++j)
                if (i < argc-2 && matchstring(argv[i+2],mapmethods[j])) {
                    mapmethod = j;
                    ++i;
                    break;
                }
            if (mapmethod < 0) mapmethod = 0;
        } else {
            fprintf(stderr,"At shell argument %d\n",i+k);
            if (k == 0)
                failure("Unrecognised or duplicate argument");
            else
                failure("Unrecognised or invalid argument value");
        }
    }

/* Check for inconsistencies, and set defaults. */

    if (trace < 0) trace = 0;
    if (method == NULL)
        failure("The -method argument is compulsory");
    if (startsize == NULL)
        failure("The -size argument is compulsory");
    decimal = 1.0;
    while (decimal < 10.0*worldprocs*(double)startsize) decimal *= 10.0;
    if (worldprocs*(double)startsize > (size_t)-1 ||
            worldprocs*decimal*startsize > 1.0/DBL_EPSILON)
        failure("The -size argument too big for this program");
    if (grouping == NULL) grouping = "w";
    if (worldprocs != power2procs && strchr(grouping,'d') != NULL)
        failure("MPI_COMM_WORLD must be 2^N for -grouping d");
    if (buffering == NULL) buffering = "d";
    if (strcmp(buffering,"d") != 0 &&
           strspn(method,"agsb") == strlen(method))
        failure("MPI's collective routines have no buffering versions");
    if (alloc && strchr(buffering,'b') == NULL)
        failure("-alloc was specified but not -buffering buffered");
    if (testtime < 0.0) testtime = 1.0;
    if (minsize > startsize)
        failure("-minsize is greater than -size");
    else if (minsize == 0)
        minsize = 1;
    if (sizefactor < 0.0)
        sizefactor = sqrt(0.5);
    else if (sizefactor < minsize/(double)startsize ||
            sizefactor > 0.999*(startsize-1)/(double)startsize)
        failure("-sizefactor does not match -size and -minsize");
    if (startprocs > worldprocs)
        failure("-procs is greater than MPI_COMM_WORLD");
    else if (startprocs < 0)
        startprocs = worldprocs;
    if (minprocs > startprocs)
        failure("-minprocs is greater than -procs");
    else if (minprocs < 0)
        minprocs = (strcmp(grouping,"w") == 0  || localprocs != 0 ?
                startprocs : 2);
    if ((startprocs > minprocs || startprocs != worldprocs) &&
            strcmp(grouping,"w") == 0)
        failure("-procs or -minprocs and -grouping world are incompatible");
    if (procfactor < 0.0)
        procfactor = sqrt(0.5);
    else if (procfactor < minprocs/(double)startprocs ||
            procfactor > 0.999*(startprocs-1)/(double)startprocs)
        failure("-procfactor does not match -procs and -minprocs");
    if (localprocs != 0) {
        if (startprocs != worldprocs || minprocs != worldprocs)
            failure("-localprocs requires -procs = -minprocs = MPI_COMM_WORLD");
        if (abs(localprocs) >= startprocs || startprocs%abs(localprocs) != 0 ||
                strcmp(grouping,"w") != 0)
            failure("-localprocs incompatible with -procs or -grouping");
    } else {
        localprocs = startprocs;
        mapmethod = 0;
    }

/* Print out what we are using. */

    if (worldid == 0) {
        fprintf(stderr,
            "Options: method=%s, grouping=%s, buffering=%s, time=%.3f\n",
                method,grouping,buffering,testtime);
        fprintf(stderr,
            "Options: worldprocs=%d, procs=%d, minprocs=%d, procfactor=%.3f\n",
                worldprocs,startprocs,minprocs,procfactor);
        fprintf(stderr,"Options: localprocs=%d %s, allocate=%d\n",
                localprocs,mapmethods[mapmethod],alloc);
        fprintf(stderr,
            "Options: size=%ld, minsizes=%ld, sizefactor=%.3f\n",
                (long)startsize,(long)minsize,sizefactor);
    }

/* Allocate the dynamic structures.  None should exceed INT_MAX in the
number of elements.  Note that sizeof returns a size_t value. */

    simplexmaplen = MAXPASSES*worldprocs;
    duplexmaplen = MAXPASSES*((worldprocs-1)/2+1);
    bcastmaplen = worldprocs*(int)(
        ceil(log(worldprocs/(double)localprocs)/log(2.0))+
        ceil((double)(log((double)localprocs)/log(2.0))));
    cpulist = myalloc("cpulist",worldprocs*sizeof(int));
    mappairs = myalloc("mappairs",2*worldprocs*worldprocs*sizeof(int));
    maptotal = myalloc("maptotal",simplexmaplen*sizeof(int));
    k = simplexmaplen*((worldprocs-1)/abs(localprocs)+1);
    mapext = myalloc("mapext",k*sizeof(int));
    mapoverall = myalloc("mapoverall",k*sizeof(int));
    k = simplexmaplen*worldprocs;
    simplexsendmap = myalloc("simplexsendmap",k*sizeof(int));
    simplexrecvmap = myalloc("simplexrecvmap",k*sizeof(int));
    duplexmap = myalloc("duplexmap",duplexmaplen*worldprocs*sizeof(int));
    sequence = myalloc("sequence",worldprocs*sizeof(int));
    bcastpairs = myalloc("bcastpairs",2*worldprocs*sizeof(int));
    bcastsendmap = myalloc("bcastsendmap",bcastmaplen*sizeof(int));
    bcastrecvmap = myalloc("bcastrecvmap",bcastmaplen*sizeof(int));
    global_status = myalloc("global_status",2*worldprocs*sizeof(MPI_Status));
    global_request = myalloc("global_request",2*worldprocs*sizeof(MPI_Request));
    k = (worldprocs-1)/abs(localprocs)+1;
    anal_nodes = myalloc("anal_nodes",k*sizeof(analysis_type));
    anal_cpus = myalloc("anal_cpus",worldprocs*sizeof(analysis_type));
    if (cpulist == NULL || mappairs == NULL || maptotal == NULL ||
            mapext == NULL || mapoverall == NULL ||
            simplexsendmap == NULL || simplexrecvmap == NULL ||
            duplexmap == NULL || sequence == NULL || bcastpairs == NULL ||
            bcastsendmap == NULL || bcastrecvmap == NULL ||
            global_status == NULL || global_request == NULL ||
            anal_nodes == NULL || anal_cpus == NULL)
        failure("Unable to get workspace");

/* Diagnose the more complicated sizes and allocate buffers that are
likely to exceed INT_MAX in size. */

    if (trace)
        fprintf(stderr,
            "Workspace sizes for process %d: %ld, %ld, %ld\n",
            worldid,(long)simplexmaplen,(long)duplexmaplen,
            (long)bcastmaplen);
    z = worldprocs*startsize*sizeof(double);
    if ((z/worldprocs)/startsize != sizeof(double) ||
            (sendbuf = myalloc("sendbuf",z)) == NULL ||
            (recvbuf = myalloc("recvbuf",z)) == NULL)
        failure("Unable to get buffer space");
    attachsize = (strpbrk(method,"hdtw") != NULL ? worldprocs : 1)*
        startsize*sizeof(double);
    if (attachsize < MINBUFFER) attachsize = MINBUFFER;
    if (alloc) {
        if ((attachbuf = myalloc("attachbuf",attachsize)) == NULL)
            failure("Unable to allocate attachable buffer");
        if (MPI_Buffer_attach(attachbuf,attachsize) != 0)
            failure("Unable to attach buffer");
    }
    if (trace)
        fprintf(stderr,
            "Buffer details for process %d: %p(%ld), %p(%ld), %p(%ld)\n",
            worldid,sendbuf,(long)z,recvbuf,(long)z,attachbuf,attachsize);

/* If selected, check the allocation of MPI processes to nodes, in some
rather vague sense. */

#ifdef USE_GETCPUID
    if (localprocs != startprocs) {
        l = getcpuid(worldid);
        k = MPI_Gather(&l,1,MPI_INT,cpulist,1,MPI_INT,0,MPI_COMM_WORLD);
        if (k) failure("MPI_Gather");
        k = 0;
        if (worldid == 0 && localprocs > 0)
            for (i = 0; i < worldprocs; i += localprocs) {
                for (j = i+1; j < localprocs; ++j)
                    if (cpulist[j] != cpulist[i]) {
                        fprintf(stderr,
                            "Process %d on different node from %d: %u, %u\n",
                            j,i,cpulist[j],cpulist[i]);
                        k = 1;
                    }
            }
        else if (worldid == 0)
            for (i = 0; i < worldprocs/(-localprocs); ++i) {
                for (j = i-localprocs; j < worldprocs; j += -localprocs)
                    if (cpulist[j] != cpulist[i]) {
                        fprintf(stderr,
                            "Process %d on different node from %d: %u, %u\n",
                            j,i,cpulist[j],cpulist[i]);
                        k = 1;
                    }
            }
    if (k) failure("Invalid allocation of processes to nodes");
    }
#endif

/* Start the loops and do the selected tests. */

    if (MPI_Barrier(MPI_COMM_WORLD)) failure("MPI_Comm_barrier");
    for (i = 0; i < COUNT(bufferings); ++i) {
        if (strchr(buffering,bufferings[i].tag[0]) == NULL)
            continue;
        global_buffering = bufferings[i];
        for (j = 0; j < COUNT(methods); ++j) {
            if (strchr(method,methods[j].tag[0]) == NULL)
                continue;
            global_method = methods[j];
            for (k = 0; k < COUNT(groupings); ++k) {
                if (strchr(grouping,groupings[k].tag[0]) == NULL)
                    continue;
                global_grouping = groupings[k];
                if (k == 0) {
                    for (i1 = 0; i1 < worldprocs; ++i1) cpulist[i1] = i1;
                    choosemap(startprocs,0);
                    choosemap(startprocs,1);
                    choosemap(startprocs,2);
                    choosecast(startprocs);
                    if (trace) {
                        displaymap(stderr,0);
                        displaymap(stderr,1);
                        displaymap(stderr,2);
                        displaymap(stderr,3);
                    }

/* For grouping world, do the tests in linear order, reversed order (if
MPI_COMM_WORLD is a power of two) and three random orders.  Note that
this must behave identically in all processes.  Otherwise, just
subdivide. */

                    iterate(sendbuf,recvbuf,MPI_COMM_WORLD);
                    if (worldprocs == power2procs) {
                        for (i1 = 0; i1 < worldprocs; ++i1)
                            cpulist[i1] = reverse(i1);
                        iterate(sendbuf,recvbuf,MPI_COMM_WORLD);
                    }
                    for (k1 = 0; k1 < 3; ++k1) {
                        for (i1 = 0; i1 < worldprocs; ++i1) {
                            if ((j1 = (int)(randy()*(i1+1))) >= i1) j1 = i1;
                            l = cpulist[i1];
                            cpulist[i1] = cpulist[j1];
                            cpulist[j1] = l;
                        }
                        iterate(sendbuf,recvbuf,MPI_COMM_WORLD);
                    }
                } else
                    subdivide(sendbuf,recvbuf);
                if (MPI_Barrier(MPI_COMM_WORLD))
                    failure("MPI_Comm_barrier");
            }
        }
    }

/* Finish off and return.  Note that this doesn't free storage. */

    if (alloc && MPI_Buffer_detach(&q,&i) != 0)
        failure("Unable to detach buffer");
    if (MPI_Finalize()) {
        fprintf(stderr,"Unable to call MPI_Finalize\n");
        exit(EXIT_FAILURE);
    }
    myfree();
    return 0;
}
