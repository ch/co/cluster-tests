#include <stdio.h>
#include <malloc.h>
#include <unistd.h>

/* Various ways to allocate a GB of memory. */

/* Stack memory: */

#define BIG_SIZE (1024*1024)
#ifndef NUM_MEGS
#define NUM_MEGS 3096
#endif

#ifdef STACK

/* Circular dependency means we need a prototype: */
int stack_function (int depth);

/* some compilers will optimise out recursion so we have two
 * stack_functions */

int second_stack_function (int depth)
{
    char array[BIG_SIZE];
    int  i = 0;

    for (i=0; i<BIG_SIZE; ++i)
        array[i] = (char) (i &0xFF);

    printf("Allocated %d Mb\n",depth +1);
    if (depth < NUM_MEGS)
        i = stack_function (++depth);

    /* Ensure the array is not optimised away! */
    for (i=0; i<BIG_SIZE; ++i)
        array[i] = 0;
    
    return 0;
}


int stack_function (int depth)
{
    char array[BIG_SIZE];
    int  i = 0;
    
    for (i=0; i<BIG_SIZE; ++i)
        array[i] = (char) (i &0xFF);

    printf("Allocated %d Mb\n",depth +1);
    if (depth < NUM_MEGS)
        i = second_stack_function (++depth);

    /* Ensure the array is not optimised away! */
    for (i=0; i<BIG_SIZE; ++i)
        array[i] = 0;

    return 0;
}

int main (int argc, char ** argv)
{
    return stack_function(0);
}

#else /* (do heap) */
/* ... or heap memory */

int main (int argc, char **argv)
{
    char * array[NUM_MEGS];
    int i = 0;
    int j = 0;

    for (i=0; i<NUM_MEGS; ++i) {
        array[i] = (char *) malloc(BIG_SIZE);
        printf("%d Mb allocated\n",(i+ 1));
    }

    /* for (i=0; i<NUM_MEGS; ++i)
        array[i] = (char *) malloc(BIG_SIZE);  */
    printf("Finished allocation\n");

    for (i=0; i<(NUM_MEGS); ++i) {
        printf("Writing Mb %d\n",i+1);
        for (j=0; j<BIG_SIZE; ++j) {
            /* printf("Writing byte %d of Mb %d\n",j+1,i+1); */
            * (array[i]+j) = (char) (j & 0xFF);
	}
    }
    printf("Written all of first lot\n");
    sleep(5);
    for (i=0; i<NUM_MEGS; ++i) {
        printf("Writing Mb %d\n",i+1);
        for (j=0; j<BIG_SIZE; ++j) 
            * (array[i]+j) = 0;
    }
    sleep(5);
    printf("Done\n");

    return 0;
}

#endif
