#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/resource.h>

int main(int argc, char * argv[]) {
    int error;
    int nprocs;
    int thisproc;
    struct rlimit rlp;
    char hard[20], soft[20];

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&thisproc);
    printf("Hello world, I am process %d of %d\n",thisproc,nprocs);
    getrlimit(RLIMIT_MEMLOCK,&rlp);
    if (rlp.rlim_max == RLIM_INFINITY) {
        strncpy(hard,"unlimited",10);
    } else {
        sprintf(hard,"%lu",rlp.rlim_max);
    }
    if (rlp.rlim_cur == RLIM_INFINITY) {
        strncpy(soft,"unlimited",10);
    } else {
        sprintf(soft,"%lu",rlp.rlim_cur);
    }
    if ( thisproc == 0 ) {
        printf("Process 0 memlock hard: %s soft: %s\n",hard,soft);
    }
    MPI_Finalize();
    exit(0);
    
}
