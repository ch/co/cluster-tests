#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/resource.h>

int main(int argc, char * argv[]) {
    int error;
    int nprocs;
    int thisproc;
    char name[255];
    char hard[20], soft[20];
    char * globmemsize;
    struct rlimit rlp;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&thisproc);

    printf("Hello world, I am process %d of %d\n",thisproc,nprocs);
    gethostname(name,255);
    printf("My host name is %s\n",name);

    getrlimit(RLIMIT_STACK,&rlp);
    if (rlp.rlim_max == RLIM_INFINITY) {
        strncpy(hard,"unlimited",10);
    } else {
        sprintf(hard,"%lu",rlp.rlim_max);
    }
    if (rlp.rlim_cur == RLIM_INFINITY) {
        strncpy(soft,"unlimited",10);
    } else {
        sprintf(soft,"%lu",rlp.rlim_cur);
    }
    printf("%s %d of %d: My stack limit is hard: %s, soft: %s\n",name,thisproc,nprocs,hard,soft);

    globmemsize=getenv("P4_GLOBMEMSIZE");
    if (globmemsize == NULL) {
        printf("%s %d of %d: P4_GLOBMEMSIZE not set\n",name,thisproc,nprocs);
    } else {
       printf("%s %d of %d: P4_GLOBMEMSIZE is %s\n",name,thisproc,nprocs,globmemsize);
    }
    globmemsize=getenv("FOOBAR");
    if (globmemsize == NULL) {
        printf("%s %d of %d: FOOBAR not set\n",name,thisproc,nprocs);
    } else {
       printf("%s %d of %d: FOOBAR is %s\n",name,thisproc,nprocs,globmemsize);
    }
    MPI_Finalize();
    exit(0);
    
}
